#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "delegates/customitemdelegate.h"
#include "paintwidget.h"
#include "math.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableView->setModel(&m_pointsModel);
    ui->tableView->setColumnWidth(0,40);
    ui->tableView->setColumnWidth(1,40);
    ui->tableView->setColumnWidth(2,40);
    ui->tableView->setColumnWidth(3,40);

    ui->tableView_3->setModel(&m_resultPointsModel);
    ui->tableView_3->setColumnWidth(0,40);
    ui->tableView_3->setColumnWidth(1,40);
    ui->tableView_3->setColumnWidth(2,40);
    ui->tableView_3->setColumnWidth(3,40);

    m_transformationModel.appendTransform(Matrix(4));
    m_transformationModel.appendTransform(Matrix(4));
    m_transformationModel.appendTransform(Matrix(4));
    m_transformationModel.appendTransform(Matrix(4));

    ui->listView_2->setModel(&m_transformationModel);
    ui->listView_2->setItemDelegate(new CustomItemDelegate(ui->listView_2));
    ui->listView_2->setEditTriggers(QAbstractItemView::AllEditTriggers);

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(appendTransformation()));

    m_paintWidget = new PaintWidget(NULL,&m_resultPointsModel);
    ui->verticalLayout_2->addWidget(m_paintWidget);
    //paintWgt->show();

    loadFigures();
    loadTransformations();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::appendTransformation()
{
    m_resultPointsModel.clear();
    for (int i = 0; i < m_pointsModel.rowCount(); i++)
        m_resultPointsModel.appendRow(m_pointsModel.pointAt(i)
                                      .applyTransformation(m_transformationModel.getTransformation()));

    m_paintWidget->update();
}

void MainWindow::loadFigures()
{
    m_figuresMap.clear();

    m_figuresMap["ladder"].append(Point3D(0,0,0,1));
    m_figuresMap["ladder"].append(Point3D(10,0,0,1));
    m_figuresMap["ladder"].append(Point3D(10,6,0,1));
    m_figuresMap["ladder"].append(Point3D(0,6,0,1));
    m_figuresMap["ladder"].append(Point3D(0,0,0,1));
    m_figuresMap["ladder"].append(Point3D(0,6,0,1));
    m_figuresMap["ladder"].append(Point3D(0,6,2,1));
    m_figuresMap["ladder"].append(Point3D(10,6,2,1));
    m_figuresMap["ladder"].append(Point3D(10,6,0,1));
    m_figuresMap["ladder"].append(Point3D(10,6,2,1));
    m_figuresMap["ladder"].append(Point3D(10,4,2,1));
    m_figuresMap["ladder"].append(Point3D(0,4,2,1));
    m_figuresMap["ladder"].append(Point3D(0,6,2,1));
    m_figuresMap["ladder"].append(Point3D(0,4,2,1));
    m_figuresMap["ladder"].append(Point3D(0,4,4,1));
    m_figuresMap["ladder"].append(Point3D(10,4,4,1));
    m_figuresMap["ladder"].append(Point3D(10,4,2,1));
    m_figuresMap["ladder"].append(Point3D(10,4,4,1));
    m_figuresMap["ladder"].append(Point3D(10,2,4,1));
    m_figuresMap["ladder"].append(Point3D(0,2,4,1));
    m_figuresMap["ladder"].append(Point3D(0,4,4,1));
    m_figuresMap["ladder"].append(Point3D(0,2,4,1));
    m_figuresMap["ladder"].append(Point3D(0,2,6,1));
    m_figuresMap["ladder"].append(Point3D(10,2,6,1));
    m_figuresMap["ladder"].append(Point3D(10,2,4,1));
    m_figuresMap["ladder"].append(Point3D(10,2,6,1));
    m_figuresMap["ladder"].append(Point3D(10,0,6,1));
    m_figuresMap["ladder"].append(Point3D(0,0,6,1));
    m_figuresMap["ladder"].append(Point3D(0,2,6,1));
    m_figuresMap["ladder"].append(Point3D(0,0,6,1));
    m_figuresMap["ladder"].append(Point3D(0,0,0,1));
    m_figuresMap["ladder"].append(Point3D(10,0,0,1));
    m_figuresMap["ladder"].append(Point3D(10,0,6,1));
    m_figuresMap["ladder"].append(Point3D(10,0,0,1));
    ui->comboBox->addItem("ladder");

    m_figuresMap["Piramid"].append(Point3D(0,0,0,1));
    m_figuresMap["Piramid"].append(Point3D(0,0,1,1));
    m_figuresMap["Piramid"].append(Point3D(1,0,0,1));
    m_figuresMap["Piramid"].append(Point3D(0,0,0,1));
    m_figuresMap["Piramid"].append(Point3D(0,0,1,1));
    m_figuresMap["Piramid"].append(Point3D(0,1,0,1));
    m_figuresMap["Piramid"].append(Point3D(1,0,0,1));
    m_figuresMap["Piramid"].append(Point3D(0,0,0,1));
    m_figuresMap["Piramid"].append(Point3D(0,1,0,1));
    m_figuresMap["Piramid"].append(Point3D(0,0,0,1));
    ui->comboBox->addItem("Piramid");

    m_figuresMap["squad"].append(Point3D(0,0,1,1));
    m_figuresMap["squad"].append(Point3D(0,1,1,1));
    m_figuresMap["squad"].append(Point3D(1,1,0,1));
    m_figuresMap["squad"].append(Point3D(1,0,0,1));
    m_figuresMap["squad"].append(Point3D(0,0,1,1));
    ui->comboBox->addItem("squad");

    double r = 3.0;

    for (double i = 0; i < 50; i+=0.1) {
        m_figuresMap["spiral"].append(Point3D(r*cos(i),r*sin(i),i/2,1));
    }
    ui->comboBox->addItem("spiral");

    on_comboBox_activated(ui->comboBox->currentText());
}

void MainWindow::loadTransformations()
{

    m_transformationMap.clear();

    Matrix m3(4);
    m3.m_numbers[0][0]= 0.707;
    m3.m_numbers[0][1]= -0.5;
    m3.m_numbers[0][2]= 0;
    m3.m_numbers[0][3]= 0;

    m3.m_numbers[1][0]= 0;
    m3.m_numbers[1][1]= 0.866;
    m3.m_numbers[1][2]= 0;
    m3.m_numbers[1][3]= 0;

    m3.m_numbers[2][0]= -0.707;
    m3.m_numbers[2][1]= -0.5;
    m3.m_numbers[2][2]= 0;
    m3.m_numbers[2][3]= 0;

    m3.m_numbers[3][0]= 0;
    m3.m_numbers[3][1]= 0;
    m3.m_numbers[3][2]= 0;
    m3.m_numbers[3][3]= 1;
    m_transformationMap["Izometria"].append(m3);
    ui->comboBox_2->addItem("Izometria");

    Matrix m2(4);
    m2.m_numbers[0][0]= 1;
    m2.m_numbers[0][1]= 0;
    m2.m_numbers[0][2]= 0;
    m2.m_numbers[0][3]= 0;

    m2.m_numbers[1][0]= 0;
    m2.m_numbers[1][1]= 1;
    m2.m_numbers[1][2]= 0;
    m2.m_numbers[1][3]= 0;

    m2.m_numbers[2][0]= -0.5;
    m2.m_numbers[2][1]= -0.5;
    m2.m_numbers[2][2]= 0;
    m2.m_numbers[2][3]= 0;

    m2.m_numbers[3][0]= 0;
    m2.m_numbers[3][1]= 0;
    m2.m_numbers[3][2]= 0;
    m2.m_numbers[3][3]= 1;
    m_transformationMap["Dimetria"].append(m2);
    ui->comboBox_2->addItem("Dimetria");


    Matrix m4(4);
    m4.m_numbers[2][2] = 0;
    m_transformationMap["Front"].append(m4);
    ui->comboBox_2->addItem("Front");

    Matrix m5(4);
    m5.m_numbers[0][0]= 0;
    m5.m_numbers[2][0]= -1;

    m_transformationMap["Side"].append(m5);
    ui->comboBox_2->addItem("Side");

    Matrix m6(4);
    m6.m_numbers[1][1]= 0;
    m6.m_numbers[2][1]= -1;

    m_transformationMap["Above"].append(m6);
    ui->comboBox_2->addItem("Above");

    ui->comboBox_2->addItem("custom");

    ui->comboBox_2->addItem("add custom");

    ui->comboBox_2->addItem("clear custom");


    on_comboBox_2_activated(ui->comboBox_2->currentText());
}

void MainWindow::on_comboBox_2_activated(const QString &arg1)
{
    if (arg1 == "add custom") {
        m_transformationMap["custom"].append(Matrix(4));
        ui->comboBox_2->setCurrentText("custom");
        on_comboBox_2_activated("custom");
        return;
    }
    if (arg1 == "clear custom") {
        m_transformationMap["custom"].clear();
    }
    m_transformationModel.clear();
    for (int i = 0; i < m_transformationMap[arg1].count(); i++)
        m_transformationModel.appendTransform(m_transformationMap[arg1].at(i));
    appendTransformation();

}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    m_pointsModel.clear();
    for (int i = 0; i < m_figuresMap[arg1].count(); i++){
        m_pointsModel.appendRow(m_figuresMap[arg1].at(i));
    }
    appendTransformation();
}
