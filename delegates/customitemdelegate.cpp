#include "customitemdelegate.h"
//#include <QSpinBox>
//#include <QPushButton>
#include <QDebug>
#include "../tableeditorwidget.h"
#include <QPainter>
#include <QPicture>
#include "../models/transformationlistmodel.h"

CustomItemDelegate::CustomItemDelegate(QObject *parent)
{
    Q_UNUSED(parent);
}

QWidget *CustomItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    TableEditorWidget *editor = new TableEditorWidget(parent);
    return editor;
}

void CustomItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Matrix m = qvariant_cast<Matrix>(index.model()->data(index, Qt::EditRole));

    painter->drawPixmap(option.rect.x()+10,option.rect.y(),16,150
                         ,QPixmap(":/img/left"));

    painter->drawPixmap(option.rect.x()+sizeHint(option,index).width() - 25,option.rect.y(),16,150
                         ,QPixmap(":/img/right"));

    painter->drawText(option.rect.x(),option.rect.y() + 80, "X");

    int max = 5;
    for (int i(0); i < 4; ++i)
        for (int k(0); k < 4; ++k){
            if (max < QString::number(m.m_numbers[i][k]).count())
                max = QString::number(m.m_numbers[i][k]).count();
        }

    for (int i(0); i < 4; ++i)
        for (int k(0); k < 4; ++k){
            painter->drawText(option.rect.x()+(max*6)*(k+1),option.rect.y()+ 30*(i+1)
                              , QString::number(m.m_numbers[i][k]));
        }
    if (index.row() == index.model()->rowCount()-1) {
        painter->drawText(option.rect.x() + sizeHint(option,index).width() - 10
                          ,option.rect.y() + 80, "=");
    }

}

void CustomItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    Matrix m = qvariant_cast<Matrix>(index.model()->data(index, Qt::EditRole));
    TableEditorWidget *itemWidget = static_cast<TableEditorWidget*>(editor);
    for (int i(0); i < 4; ++i)
        for (int k(0); k < 4; ++k){
            QTableWidgetItem *item = new QTableWidgetItem(QString::number(m.m_numbers[i][k]));
            itemWidget->setItem(i,k,item);
        }
}

void CustomItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    Matrix m(4);
    TableEditorWidget *itemWidget = static_cast<TableEditorWidget*>(editor);
    for (int i(0); i < 4; ++i)
        for (int k(0); k < 4; ++k){
            m.m_numbers[i][k] = itemWidget->item(i,k)->text().toDouble();
        }
    model->setData(index,QVariant::fromValue(m), Qt::EditRole);

}

void CustomItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QRect rect(option.rect);
    rect.setX(rect.x()+10);
    rect.setHeight(160);
    rect.setWidth(sizeHint(option,index).height());
    editor->setGeometry(rect);
}

QSize CustomItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Matrix m = qvariant_cast<Matrix>(index.model()->data(index, Qt::EditRole));
    int max = 5;
    for (int i(0); i < 4; ++i)
        for (int k(0); k < 4; ++k){
            if (max < QString::number(m.m_numbers[i][k]).count())
                max = QString::number(m.m_numbers[i][k]).count();
        }
    Q_UNUSED(option);
    return QSize(max*6*4+10+25,160);
}
