#ifndef TRANSFORMATIONLISTMODEL_H
#define TRANSFORMATIONLISTMODEL_H

#include <QAbstractListModel>
#include "../utils/point3d.h"

class TransformationListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TransformationListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex & index) const ;
    void appendTransform(Matrix matrix);
    void loadTransformation(QString fileName);
    Transformation getTransformation() const {return m_transformation;}
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    bool insertRows(int position, int rows, const QModelIndex &parent);
    bool removeRows(int position, int rows, const QModelIndex &parent);
    void clear();
signals:

public slots:
private:
    Transformation m_transformation;

};

#endif // TRANSFORMATIONLISTMODEL_H
