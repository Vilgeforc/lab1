#ifndef POINTSTABLEMODEL_H
#define POINTSTABLEMODEL_H

#include <QAbstractTableModel>
#include "../utils/point3d.h"

class PointsTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit PointsTableModel(QObject *parent = 0);
    void loadFigure(QString fileName);

    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    int columnCount(const QModelIndex &parent) const;

    QVariant headerData(int section,Qt::Orientation orientation, int role=Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void appendRow(Point3D point);
    bool insertRows(int position, int rows, const QModelIndex &parent);
    bool removeRows(int position, int rows, const QModelIndex &parent);

    Point3D pointAt(int row) const;

    void clear();

private:
    QVector <Point3D> m_pointsVector;

signals:

public slots:

};

#endif // POINTSTABLEMODEL_H
