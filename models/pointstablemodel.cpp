#include "pointstablemodel.h"
#include <QFile>
#include <QDataStream>
#include <QStringList>
#include <QSize>
#include <QDebug>

PointsTableModel::PointsTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{

}

void PointsTableModel::loadFigure(QString fileName)
{
    beginInsertRows(QModelIndex(),m_pointsVector.count(),1);
    m_pointsVector.clear();
    QFile file(fileName);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QDataStream in(&file);
    while(!file.atEnd()) {
        Point3D point;
        in >> point;
        m_pointsVector.append(point);
    }
    file.close();
    endInsertRows();
}

QVariant PointsTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_pointsVector.count())
        return QVariant();
    if (role == Qt::DisplayRole || role == Qt::EditRole){
        if (index.column() == 0 )
            return m_pointsVector[index.row()].x();
        if (index.column() == 1 )
            return m_pointsVector[index.row()].y();
        if (index.column() == 2 )
            return m_pointsVector[index.row()].z();
        if (index.column() == 3 )
            return m_pointsVector[index.row()].w();
    }
    if (role == Qt::UserRole) {
        return QVariant::fromValue(m_pointsVector[index.row()]);
    }
    return QVariant();
}

bool PointsTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        if(index.column()==0){
            m_pointsVector[index.row()].setX(value.toDouble());
        }
        if(index.column()==1){
            m_pointsVector[index.row()].setY(value.toDouble());
        }
        if(index.column()==2){
            m_pointsVector[index.row()].setZ(value.toDouble());
        }
        if(index.column()==3){
            m_pointsVector[index.row()].setW(value.toDouble());
        }

        return true;
    }
    if (role == Qt::DisplayRole) {
         m_pointsVector[index.row()] = qvariant_cast<Point3D> (value);
    }
    return false;
}

int PointsTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_pointsVector.count();
}

int PointsTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 4;
}

QVariant PointsTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QStringList headerData;
    headerData<<"x"<<"y"<<"z"<<"w";
    if(role != Qt::DisplayRole)
           return QVariant();
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole){
       return headerData.at(section); // заголовки столбцов
    }else{
       return QString("%1").arg( section + 1 ); // возвращаем номера строк
    }
}

Qt::ItemFlags PointsTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
            return Qt::ItemIsEnabled;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

void PointsTableModel::appendRow(Point3D point)
{
    insertRows(rowCount(),1,QModelIndex());
    QModelIndex a = this->index(rowCount()-1,0,QModelIndex());
    setData(a,QVariant::fromValue(point),Qt::DisplayRole);
}

bool PointsTableModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        m_pointsVector.insert(position,Point3D());
    }

    endInsertRows();
    return true;
}

bool PointsTableModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        m_pointsVector.remove(position);
    }

    endRemoveRows();
    return true;
}

Point3D PointsTableModel::pointAt(int row) const
{
    if (row > m_pointsVector.count() || row < 0) return Point3D(0,0,0,0);
    return m_pointsVector[row];
}

void PointsTableModel::clear()
{
    removeRows(0,rowCount(),QModelIndex());
}
