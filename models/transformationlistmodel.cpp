#include "transformationlistmodel.h"
#include <QDebug>

TransformationListModel::TransformationListModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

int TransformationListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_transformation.count();
}

QVariant TransformationListModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        return QVariant(index.row());
    }
    if (role == Qt::EditRole) {
        return QVariant::fromValue(m_transformation[index.row()]);
    }
    return QVariant();
}

Qt::ItemFlags TransformationListModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
}

void TransformationListModel::appendTransform(Matrix matrix)
{
    insertRows(rowCount(),1,QModelIndex());
    QModelIndex a = this->index(rowCount()-1,0,QModelIndex());
    setData(a,QVariant::fromValue(matrix),Qt::EditRole);
}

void TransformationListModel::loadTransformation(QString fileName)
{

}

bool TransformationListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        Matrix m = qvariant_cast<Matrix> (value);
        m_transformation[index.row()].m_numbers = m.m_numbers;
        emit dataChanged(index, index);
        return true;
    }
    return false;

    return true;
}

bool TransformationListModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        m_transformation.insert(position,Matrix(4));
    }

    endInsertRows();
    return true;
}

bool TransformationListModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        m_transformation.remove(position);
    }

    endRemoveRows();
    return true;
}

void TransformationListModel::clear()
{
    removeRows(0,rowCount(),QModelIndex());
}
