#include "tableeditorwidget.h"

TableEditorWidget::TableEditorWidget(QWidget *parent) :
    QTableWidget(parent)
{
    this->setRowCount(4);
    this->setColumnCount(4);
    for (int i = 0; i < 4; i++) {
        this->setRowHeight(i,30);
        this->setColumnWidth(i,30);
    }
}
