#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "models/pointstablemodel.h"
#include "models/transformationlistmodel.h"
#include <QMap>
#include "paintwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    PointsTableModel m_pointsModel;
    PointsTableModel m_resultPointsModel;
    TransformationListModel m_transformationModel;
public slots:
    void appendTransformation();
    void loadFigures();
    void loadTransformations();
private slots:
    void on_comboBox_2_activated(const QString &arg1);

    void on_comboBox_activated(const QString &arg1);

private:
    QMap <QString, Transformation> m_transformationMap;
    QMap <QString, QVector<Point3D> > m_figuresMap;
    PaintWidget *m_paintWidget;
};

#endif // MAINWINDOW_H
