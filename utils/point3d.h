#ifndef POINT3D_H
#define POINT3D_H

#include <QDataStream>
#include "matrix.h"

class Point3D
{
public:
    Point3D(){}
    Point3D(double x, double y, double z, double w = 1) : m_x(x),m_y(y),m_z(z),m_w(w){}


    Point3D applyTransformation(Transformation transformation);

    friend QDataStream &operator<<(QDataStream &out,const Point3D point);

    friend QDataStream &operator>>(QDataStream &in, Point3D point);

    double x() const;
    void setX(double x);
    double y() const;
    void setY(double y);
    double z() const;
    void setZ(double z);
    double w() const;
    void setW(double w);
private:
    double m_x;
    double m_y;
    double m_z;
    double m_w;
};

Q_DECLARE_METATYPE(Point3D)
#endif // POINT3D_H
