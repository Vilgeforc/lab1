#ifndef MATRIX_H
#define MATRIX_H

#include <QVector>
#include <QVariant>

class Matrix
{
public:
    Matrix();

    Matrix(int dimension){
        for (int i = 0; i < dimension; i++){
            QVector<double> buf;
            for (int k = 0; k < dimension; k++)
                if (i == k)
                    buf.push_back(1);
                else
                    buf.push_back(0);
            m_numbers.push_back(buf);
        }
    }
    QVector < QVector<double> > m_numbers;

};
Q_DECLARE_METATYPE(Matrix)
#define Transformation QVector <Matrix>
#endif // MATRIX_H
