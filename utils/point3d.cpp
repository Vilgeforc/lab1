#include "point3d.h"

double Point3D::y() const
{
    return m_y;
}

void Point3D::setY(double y)
{
    m_y = y;
}

double Point3D::z() const
{
return m_z;
}

void Point3D::setZ(double z)
{
m_z = z;
}

double Point3D::w() const
{
return m_w;
}

void Point3D::setW(double w)
{
m_w = w;
}

Point3D Point3D::applyTransformation(Transformation transformation)
{
    double x1 = x();
    double y1 = y();
    double z1 = z();
    double w1 = w();

    double x2,y2,z2,w2;
    for (int i = 0; i < transformation.count(); i++) {
        x2 =      x1 * transformation[i].m_numbers[0][0]
                + y1 * transformation[i].m_numbers[1][0]
                + z1 * transformation[i].m_numbers[2][0]
                + w1 * transformation[i].m_numbers[3][0];
        y2 =      x1 * transformation[i].m_numbers[0][1]
                + y1 * transformation[i].m_numbers[1][1]
                + z1 * transformation[i].m_numbers[2][1]
                + w1 * transformation[i].m_numbers[3][1];
        z2 =      x1 * transformation[i].m_numbers[0][2]
                + y1 * transformation[i].m_numbers[1][2]
                + z1 * transformation[i].m_numbers[2][2]
                + w1 * transformation[i].m_numbers[3][2];
        w2 =      x1 * transformation[i].m_numbers[0][3]
                + y1 * transformation[i].m_numbers[1][3]
                + z1 * transformation[i].m_numbers[2][3]
                + w1 * transformation[i].m_numbers[3][3];
        x1 = x2;
        y1 = y2;
        z1 = z2;
        w1 = w2;
    }
    return Point3D(x1,y1,z1,w1);
}

QDataStream &operator<<(QDataStream &out,const Point3D point)
{
    out<<point.x()<<point.y()<<point.z()<<point.w();
    return out;
}

QDataStream &operator>>(QDataStream &in, Point3D point)
{
    double x,y,z,w;
    in>>x>>y>>z>>w;
    point.setX(x);
    point.setY(y);
    point.setZ(z);
    point.setW(w);
    return in;
}

double Point3D::x() const
{
    return m_x;
}

void Point3D::setX(double x)
{
    m_x = x;
}
