#-------------------------------------------------
#
# Project created by QtCreator 2014-02-19T19:10:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tableeditorwidget.cpp \
    delegates/customitemdelegate.cpp \
    models/transformationlistmodel.cpp \
    models/pointstablemodel.cpp \
    utils/point3d.cpp \
    utils/matrix.cpp \
    paintwidget.cpp

HEADERS  += mainwindow.h \
    tableeditorwidget.h \
    delegates/customitemdelegate.h \
    models/transformationlistmodel.h \
    models/pointstablemodel.h \
    utils/point3d.h \
    utils/matrix.h \
    paintwidget.h
FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
