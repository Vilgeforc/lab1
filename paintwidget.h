#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPaintEvent>
#include "models/pointstablemodel.h"

class PaintWidget : public QLabel
{
    Q_OBJECT
public:
    explicit PaintWidget(QWidget *parent = 0
            ,PointsTableModel *model = new PointsTableModel() ) : m_model(*model){
        Q_UNUSED(parent);
        setMinimumSize(200,200);
    }
    void paintEvent(QPaintEvent *);
signals:

public slots:

private:
    PointsTableModel &m_model;

};

#endif // PAINTWIDGET_H
