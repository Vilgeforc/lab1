#include "paintwidget.h"
#include <QPainter>
#include <QDebug>
#include <QPen>
#include <float.h>


void PaintWidget::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QRect rect(this->rect());
    rect.setWidth(rect.width()-1);
    rect.setHeight(rect.height() - 1);
    p.drawRect(rect);

    QPen axisPen(Qt::gray, 1);
    axisPen.setStyle(Qt::DashLine);

    p.setPen(axisPen);

    p.drawLine(rect.topLeft().x(),rect.y()+rect.height()/2
              ,rect.topRight().x()-1, rect.y()+rect.height()/2);
    p.drawLine(rect.topLeft().x()+rect.width()/2, rect.topLeft().y()
              ,rect.topLeft().x()+rect.width()/2, rect.bottomLeft().y());

    if (m_model.rowCount() == 0) return;
    p.setPen(Qt::SolidLine);
    double minX = DBL_MAX,maxX = -DBL_MAX
          ,minY = DBL_MAX,maxY = -DBL_MAX;

    int factor = qMin(rect.width(),rect.height())/2.88;

    for (int i = 0; i< m_model.rowCount()-1; i++){
        Point3D point = qvariant_cast<Point3D> (m_model.data(m_model.index(i,0,QModelIndex() ),Qt::UserRole));
        double x = point.x()/point.w();
        double y = point.y()/point.w();
        if (x < minX) minX = x;
        if (x > maxX) maxX = x;
        if (y < minY) minY = y;
        if (y > maxY) maxY = y;

    }

    double delta = qMax(maxX - minX, maxY - minY);
    for (int i = 0; i< m_model.rowCount()-1; i++){
        Point3D point = qvariant_cast<Point3D> (m_model.data(m_model.index(i,0,QModelIndex() ),Qt::UserRole));
        double x = point.x()/point.w()*factor/(delta);
        double y = point.y()/point.w()*factor/(delta);

        Point3D point1 = qvariant_cast<Point3D> (m_model.data(m_model.index(i+1,0,QModelIndex() ),Qt::UserRole));
        double x1 = point1.x()/point1.w()*factor/(delta);
        double y1 = point1.y()/point1.w()*factor/(delta);

        p.drawLine(rect.width()/2+x,rect.height()/2-y,rect.width()/2+x1,rect.height()/2-y1);
    }
}
